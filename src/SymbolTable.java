//
//  SYMBOL TABLE. A symbol table for the Snarl compiler.
//
//    James B. Moen
//    07 Dec 14
//

//  DESCRIPTOR. Information about a name. We'll add stuff here later.

class Descriptor
{
    private Type type; //A type from the type system
    public Type getType()
    {
        return type; // 4S1 - add moree stuff.
    }
    public Descriptor(Type type)
    {
        this.type = type;
    }
}

//  SYMBOL TABLE. Map names to DESCRIPTORs by nesting level, using a hash table
//  of stacks. We implement our own hash table and linked stacks.

class SymbolTable
{
  private static final int modulus = 997;  //  A "large" prime.

//  ENTRY. A node in a linked stack of NAME-DESCRIPTOR pairs. We assume NAME is
//  never NULL. ENTRYs in a linked stack appear in nondecreasing order of LEVEL
//  slots.

  private class Entry
  {
    private String     name;        //  A name from a program.
    private Descriptor descriptor;  //  DESCRIPTOR for NAME.
    private int        level;       //  Nesting level for NAME.
    private Entry      next;        //  Next ENTRY in the stack.

//  Constructor. Make a new ENTRY with given slots.

    private Entry(String name, Descriptor descriptor, int level, Entry next)
    {
      this.name       = name;
      this.descriptor = descriptor;
      this.level      = level;
      this.next       = next;
    }
  }

  private int      level;  //  Current nesting level.
  private Entry [] table;  //  Hash table of linked stacks.

//  Constructor. Make a new empty SYMBOL TABLE.

  public SymbolTable()
  {
    level = 0;
    table = new Entry[modulus];
  }

//  GET DESCRIPTOR. If NAME is not associated with a DESCRIPTOR, then we return
//  NULL. Otherwise return the DESCRIPTOR associated with NAME.

  public Descriptor getDescriptor(String name)
  {
    Entry entry = table[hash(name)];
    while (true)
    {
      if (entry == null)
      {
        return null;
      }
      else if (entry.name.equals(name))
      {
        return entry.descriptor;
      }
      else
      {
        entry = entry.next;
      }
    }
  }

//  HASH. Return an index into the hash table TABLE. The bitwise AND forces the
//  HASH CODE for NAME to be nonnegative by zeroing out its high bit.

  private int hash(String name)
  {
    return (Integer.MAX_VALUE & name.hashCode()) % modulus;
  }

//  IS EMPTY. Test if we're not in a scope, so the symbol table is empty.

  public boolean isEmpty()
  {
    return level == 0;
  }

//  POP. Assert that we've exited a lexical scope. If we're not inside a scope,
//  then throw an exception.

  public void pop()
  {
    if (isEmpty())
    {
      throw new IllegalStateException("Symbol table is empty.");
    }
    else
    {
      Entry entry;
      for (int index = 0; index < modulus; index += 1)
      {
        entry = table[index];
        while (entry != null && entry.level == level)
        {
          entry = entry.next;
        }
        table[index] = entry;
      }
      level -= 1;
    }
  }

//  PUSH. Assert that we've entered a lexical scope.

  public void push()
  {
    level += 1;
  }

//  SET DESCRIPTOR. Associate NAME and DESCRIPTOR in the current lexical scope.
//  If we're not in a scope, then throw an exception. If we're successful, then
//  return TRUE. If we're unsuccessful, because NAME is already associated with
//  a DESCRIPTOR in the current scope, then return FALSE.

  public boolean setDescriptor(String name, Descriptor descriptor)
  {
    if (isEmpty())
    {
      throw new IllegalStateException("Symbol table is empty.");
    }
    else
    {
      int index = hash(name);
      Entry entry = table[index];
      while (true)
      {
        if (entry == null || entry.level < level)
        {
          table[index] = new Entry(name, descriptor, level, table[index]);
          return true;
        }
        else if (entry.level == level && entry.name.equals(name))
        {
          return false;
        }
        else
        {
          entry = entry.next;
        }
      }
    }
  }
}

