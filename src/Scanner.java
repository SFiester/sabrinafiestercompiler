//
//  SNARL/SCANNER. Token scanner.
//
//    James Moen
//    01 Dec 14
//

import java.util.Hashtable;

//  SCANNER. Snarl source language scanner.

class Scanner extends Source
{
  private Hashtable<String, Integer> reservedNames;  //  Map names to tokens.
  private int                        token;          //  Current token.
  private int                        tokenInteger;   //  TOKEN as an INT.
  private String                     tokenString;    //  TOKEN as a STRING.

//  Constructor. Make a new scanner that reads tokens from SOURCE.

  public Scanner(String path)
  {
    super(path);
    reservedNames = new Hashtable<String, Integer>();
    reservedNames.put("and",    new Integer(boldAndToken));
    reservedNames.put("begin",  new Integer(boldBeginToken));
    reservedNames.put("code",   new Integer(boldCodeToken));
    reservedNames.put("do",     new Integer(boldDoToken));
    reservedNames.put("else",   new Integer(boldElseToken));
    reservedNames.put("end",    new Integer(boldEndToken));
    reservedNames.put("if",     new Integer(boldIfToken));
    reservedNames.put("int",    new Integer(boldIntToken));
    reservedNames.put("or",     new Integer(boldOrToken));
    reservedNames.put("not",    new Integer(boldNotToken));
    reservedNames.put("proc",   new Integer(boldProcToken));
    reservedNames.put("string", new Integer(boldStringToken));
    reservedNames.put("then",   new Integer(boldThenToken));
    reservedNames.put("value",  new Integer(boldValueToken));
    reservedNames.put("while",  new Integer(boldWhileToken));
    tokenInteger = 0;
    tokenString = "";
    nextToken();
  }

//  GET INTEGER. Return the INT value of TOKEN, if it exists.

  public int getInteger()
  {
    return tokenInteger;
  }

//  GET STRING. Return the STRING value of TOKEN, if it exists.

  public String getString()
  {
    return tokenString;
  }

//  GET TOKEN. Return the current token.

  public int getToken()
  {
    return token;
  }

//  IS DIGIT. Test if CH is a decimal digit.

  private boolean isDigit(char ch)
  {
    return '0' <= ch && ch <= '9';
  }

//  IS LETTER. Test if CH is a Roman letter.

  private boolean isLetter(char ch)
  {
    return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z';
  }

//  IS LETTER OR DIGIT. Test if CH is a decimal digit or a Roman letter.

  private boolean isLetterOrDigit(char ch)
  {
    return isLetter(ch) || isDigit(ch);
  }

//  MAIN. For testing.

  public static void main(String[] files)
  {
    Scanner scanner = new Scanner(files[0]);
    while (scanner.getToken() != endFileToken)
    {
      System.out.print(tokenToString(scanner.getToken()));
      switch (scanner.getToken())
      {
        case intConstantToken:
        {
          System.out.print(" " + scanner.getInteger());
          break;
        }
        case nameToken:
        case stringConstantToken:
        {
          System.out.print(" \"" + scanner.getString() + "\"");
          break;
        }
      }
      System.out.println();
      scanner.nextToken();
    }
  }

//  NEXT TOKEN. Get the next token from READER.

  public void nextToken()
  {
    token = ignoredToken;
    while (token == ignoredToken)
    {
      if (isLetter(getChar()))
      {
        nextName();
      }
      else if (isDigit(getChar()))
      {
        nextIntConstant();
      }
      else
      {
        switch (getChar())
        {
          case eofChar: { nextEndFile();                 break; }
          case ' ':     { nextChar();                    break; }
          case '"':     { nextStringConstant();          break; }
          case '#':     { nextComment();                 break; }
          case '(':     { nextSingle(openParenToken);    break; }
          case ')':     { nextSingle(closeParenToken);   break; }
          case '*':     { nextSingle(starToken);         break; }
          case '+':     { nextSingle(plusToken);         break; }
          case ',':     { nextSingle(commaToken);        break; }
          case '-':     { nextSingle(dashToken);         break; }
          case '/':     { nextSingle(slashToken);        break; }
          case ':':     { nextColon();                   break; }
          case ';':     { nextSingle(semicolonToken);    break; }
          case '<':     { nextLess();                    break; }
          case '=':     { nextSingle(equalToken);        break; }
          case '>':     { nextGreater();                 break; }
          case '[':     { nextSingle(openBracketToken);  break; }
          case ']':     { nextSingle(closeBracketToken); break; }
          case '¬':     { nextSingle(boldNotToken);      break; }
          case '×':     { nextSingle(starToken);         break; }
          case '∧':     { nextSingle(boldAndToken);      break; }
          case '∨':     { nextSingle(boldOrToken);       break; }
          case '≠':     { nextSingle(lessGreaterToken);  break; }
          case '≤':     { nextSingle(lessEqualToken);    break; }
          case '≥':     { nextSingle(greaterEqualToken); break; }
          default:      { nextIllegal();                 break; }
        }
      }
    }
  }

//  NEXT TOKEN. If the current token is EXPECTED TOKEN, then get the next token
//  as above. If it isn't, then write an error and halt the program.

  public void nextToken(int expectedToken)
  {
    if (token == expectedToken)
    {
      nextToken();
    }
    else
    {
      error(
       "\"" + tokenToString(expectedToken) +
       "\" expected instead of \"" +
       tokenToString(token) + "\".");
    }
  }

//  NEXT COLON. Scan a COLON EQUAL TOKEN.

  private void nextColon()
  {
    nextChar();
    if (getChar() == '=')
    {
      token = colonEqualToken;
      nextChar();
    }
    else
    {
      token = colonToken;
    }
  }

//  NEXT COMMENT. Scan a comment. Skip chars until we hit the end of the line.

  private void nextComment()
  {
    while (! atLineEnd())
    {
      nextChar();
    }
    nextChar();
  }

//  NEXT END FILE. Scan an END FILE TOKEN.

  private void nextEndFile()
  {
    token = endFileToken;
  }

//  NEXT GREATER. Scan a GREATER TOKEN or a GREATER EQUAL TOKEN.

  private void nextGreater()
  {
    nextChar();
    if (getChar() == '=')
    {
      token = greaterEqualToken;
      nextChar();
    }
    else
    {
      token = greaterToken;
    }
  }

//  NEXT ILLEGAL. Scan an illegal token.

  private void nextIllegal()
  {
    error("Illegal token.");
  }

//  NEXT INT CONSTANT. Scan an INT CONSTANT TOKEN.

  private void nextIntConstant()
  { 
    token = intConstantToken;
    StringBuffer builder = new StringBuffer();
    while (isDigit(getChar()))
    {
      builder.append(getChar());
      nextChar();
    }
    try
    {
      tokenInteger = Integer.parseInt(builder.toString());
    }
    catch (NumberFormatException ignore)
    {
      error("Integer constant is too large.");
    }
  }

//  NEXT LESS. Scan a LESS TOKEN, a LESS EQUAL TOKEN, or a LESS GREATER TOKEN.

  private void nextLess()
  { 
    nextChar();
    if (getChar() == '=')
    {
      token = lessEqualToken;
      nextChar();
    }
    else if (getChar() == '>')
    { 
      token = lessGreaterToken;
      nextChar();
    }
    else
    {
      token = lessToken;
    }
  }

//  NEXT NAME. Scan a NAME TOKEN or one of the reserved tokens.

  private void nextName()
  { 
    StringBuffer buffer = new StringBuffer();
    while (isLetterOrDigit(getChar()))
    {
      buffer.append(getChar());
      nextChar();
    }
    tokenString = buffer.toString();
    if (reservedNames.containsKey(tokenString))
    {
      token = reservedNames.get(tokenString).intValue();
    }
    else
    {
      token = nameToken;
    }
  }

//  NEXT SINGLE. Scan a single character TOKEN.

  private void nextSingle(int token)
  {
    this.token = token;
    nextChar();
   }

//  NEXT STRING CONSTANT. Scan a STRING CONSTANT TOKEN.

  private void nextStringConstant()
  {
    token = stringConstantToken;
    StringBuffer buffer = new StringBuffer();
    nextChar();
    while (getChar() != '"' && ! atLineEnd())
    {
      buffer.append(getChar());
      nextChar();
    }
    if (getChar() == '"')
    {
      nextChar();
    }
    else
    {
      error("String has no closing quote.");
    }
    tokenString = buffer.toString();
  }
}
