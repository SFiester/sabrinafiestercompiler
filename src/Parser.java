//
//  SNARL/PARSER. Parse a Snarl source program.
//
//    James Moen
//    01 Dec 14
//

//  PARSER. Recursive descent parser for the Snarl programming language. It can
//  halt when it detects syntax errors, but it can't recover from them.

class Parser extends Scanner
{
    private Type intType;
    private Type stringType;
    private SymbolTable table;


//  Constructor. Make a new PARSER that reads a Snarl source from PATH.

    public Parser(String path)
    {
        super(path);
        intType = new BasicType("int", Type.wordSize, null);
        stringType = new BasicType("string", Type.wordSize, null);
        table = new SymbolTable();
        table.push();
        doPassOne();
        doPassTwo();


    }

//  NEXT EXPECTED. If the current token is TOKEN, then skip it. Otherwise issue
//  an error message that mentions TOKEN's printable name.

    private void nextExpected(int token)
    {
        if (getToken() == token)
        {
            nextToken();
        }
        else
        {
            error("Expected " + tokenToString(token) + ".");
        }
    }

//  NEXT PROGRAM. Parse a series of one or more PROGRAM PARTs, all separated by
//  semicolons. They're terminated by an end of file marker.

    private void nextProgram()
    {
        enter("nextProgram");
        nextProgramPart();
        while (getToken() == semicolonToken)
        {
            nextToken();
            nextProgramPart();
        }
        nextExpected(endFileToken);
        exit("nextProgram");
    }

//  NEXT PROGRAM PART. Parse a program part. It's either a variable declaration
//  or a procedure definition.

    private void nextProgramPart()
    {
        enter("nextProgramPart");
        switch (getToken())
        {
            case boldIntToken:
            {
                nextToken();
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(intType));
                break;
            }
            case boldProcToken:
            {
                nextProcedure();
                break;
            }
            case boldStringToken:
            {
                nextToken();
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(stringType));
                break;
            }
            case openBracketToken:
            {
                nextToken();
                int length = getInteger();
                nextExpected(intConstantToken);
                nextExpected(closeBracketToken);
                nextExpected(boldIntToken);
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(new ArrayType(length, intType)));
                break;
            }
            default:
            {
                error("Program part expected.");
                break;
            }
        }
        exit("nextProgramPart");
    }

//  NEXT PROCEDURE. Parse a procedure definition.

    private void nextProcedure()
    {
        enter("nextProcedure");
        nextToken();
        String name = getString();
        nextExpected(nameToken);
        table.push();

//  Parse the parameter list.

        nextExpected(openParenToken);
        if (getToken() != closeParenToken)
        {
            nextParameter();
            while (getToken() == commaToken)
            {
                nextToken();
                nextParameter();
            }
        }
        nextExpected(closeParenToken);

//  Parse the value type, followed by a colon.
        Type valueType;

        switch (getToken())
        {
            case boldIntToken:
            {
                valueType = intType;
                nextToken();
                break;
            }
            case boldStringToken:
            {
                valueType = stringType;
                nextToken();
                break;
            }
            default:
            {
                error("Value type expected.");
                break;
            }
        }
        nextExpected(colonToken);

//  Parse zero or more local variable declarations separated by semicolons, and
//  then a BEGIN statement.

        if (getToken() != boldBeginToken)
        {
            nextLocal();
            while (getToken() == semicolonToken)
            {
                nextToken();
                nextLocal();
            }
        }
        nextBegin();
        table.pop();
        exit("nextProcedure");
    }

//  NEXT PARAMETER. Parse a procedure parameter declaration.

    private void nextParameter()
    {
        enter("nextParameter");
        switch (getToken())
        {
            case boldIntToken:
            {
                nextToken();
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(intType));
                break;
            }
            case boldStringToken:
            {
                nextToken();
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(stringType));
                break;
            }
            case openBracketToken:
            {
                nextToken();
                int length = getInteger();
                nextExpected(intConstantToken);
                nextExpected(closeBracketToken);
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(new ArrayType(length, intType)));
                break;
            }
            default:
            {
                error("Parameter expected.");
                break;
            }
        }
        exit("nextParameter");
    }

//  NEXT LOCAL. Parse a local variable declaration.

    private void nextLocal()
    {
        enter("nextLocal");
        switch (getToken())
        {
            case boldIntToken:
            {
                nextToken();
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(intType));
                break;
            }
            case boldStringToken:
            {
                nextToken();
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(stringType));
                break;
            }
            case openBracketToken:
            {
                nextToken();
                int length = getInteger();
                nextExpected(intConstantToken);
                nextExpected(closeBracketToken);
                String name = getString();
                nextExpected(nameToken);
                table.setDescriptor(name, new Descriptor(new ArrayType(length, intType)));
                break;
            }
            default:
            {
                error("Local declaration expected.");
                break;
            }
        }
        exit("nextLocal");
    }

//  NEXT STATEMENT. Parse a statement. Note that a NAME TOKEN introduces either
//  an ordinary assignment, an array element assignment, or a procedure call.

    private void nextStatement()
    {
        enter("nextStatement");
        switch (getToken())
        {
            case boldBeginToken:
            {
                nextBegin();
                break;
            }
            case boldCodeToken:
            {
                nextCode();
                break;
            }
            case boldIfToken:
            {
                nextIf();
                break;
            }
            case boldWhileToken:
            {
                nextWhile();
                break;
            }
            case boldValueToken:
            {
                nextValue();
                break;
            }
            case nameToken:
            {
                String name = getString();
                nextToken();
                Descriptor desc = table.getDescriptor(name);

                switch (getToken())
                {
                    case colonEqualToken:
                    {
                        if (!desc.getType().isSubtype(intType) && !desc.getType().isSubtype(stringType) )
                        {
                            error("int or string type expected.");
                        }
                        nextToken();
                        Descriptor desc2 = nextExpression();
                        if (!desc2.getType().isSubtype(desc.getType()))
                        {
                            error("types not compatible");
                        }
                        break;
                    }
                    case openBracketToken:
                    {
                        if (!desc.getType().isSubtype(new ArrayType(0, intType)))
                        {
                            error("array type expected.");
                        }
                        nextToken();
                        Descriptor desc2 = nextExpression();
                        if (!desc2.getType().isSubtype(intType))
                        {
                            error("int type expected");
                        }
                        nextExpected(closeBracketToken);
                        nextExpected(colonEqualToken);
                        desc2 = nextExpression();
                        if (!desc2.getType().isSubtype(intType))
                        {
                            error("int type expected");
                        }
                        break;
                    }
                    case openParenToken:
                    {
                        if (!desc.getType().isSubtype(new ProcedureType()))
                        {
                            error("Procedure type expected");
                        }
                        nextArguments();
                        break;
                    }
                    default:
                    {
                        error("Assignment or call expected.");
                        break;
                    }
                }
                break;
            }
            default:
            {
                error("Statement expected.");
                break;
            }
        }
        exit("nextStatement");
    }

//  NEXT ARGUMENTS. Parse a series of zero or more expressions, enclosed inside
//  matching parentheses and separated by commas.

    private void nextArguments()
    {
        enter("nextArguments");
        nextToken();
        if (getToken() != closeParenToken)
        {
            nextExpression();
            while (getToken() == commaToken)
            {
                nextToken();
                nextExpression();
            }
        }

        nextExpected(closeParenToken);
        exit("nextArguments");
    }

//  NEXT BEGIN. Parse a BEGIN token, and then zero or more statements separated
//  by semicolons, terminated by an END token.

    private void nextBegin()
    {
        enter("nextBegin");
        nextToken();
        if (getToken() != boldEndToken)
        {
            nextStatement();
            while (getToken() == semicolonToken)
            {
                nextToken();
                nextStatement();
            }
        }
        nextExpected(boldEndToken);
        exit("nextBegin");
    }

//  NEXT CODE. Parse a CODE statement.

    private void nextCode()
    {
        enter("nextCode");
        nextToken();
        nextExpected(stringConstantToken);
        exit("nextCode");
    }

//  NEXT IF. Parse an IF statement. The ELSE part is optional.

    private void nextIf()
    {
        enter("nextIf");
        nextToken();
        Descriptor d = nextExpression();
        if (!d.getType().isSubtype(intType))
        {
            error("int expression expected.");
        }
        nextExpected(boldThenToken);
        nextStatement();
        if (getToken() == boldElseToken)
        {
            nextToken();
            nextStatement();
        }
        exit("nextIf");
    }

//  NEXT WHILE. Parse a WHILE statement.

    private void nextWhile()
    {
        enter("nextWhile");
        nextToken();
        Descriptor d = nextExpression();
        if (!d.getType().isSubtype(intType))
        {
            error("int type expected");
        }
        nextExpected(boldDoToken);
        nextStatement();
        exit("nextWhile");
    }

//  NEXT VALUE. Parse a VALUE statement.

    private void nextValue()
    {
        enter("nextValue");
        nextToken();
        nextExpression();
        exit("nextValue");
    }

//  NEXT EXPRESSION. Parse one or more CONJUNCTIONs, separated by OR tokens. It
//  might also be called NEXT DISJUNCTION, but it isn't.

    private Descriptor nextExpression()
    {
        enter("nextExpression");
        Descriptor d = nextConjunction();
        while (getToken() == boldOrToken)
        {
            nextToken();
            d = nextConjunction();
        }
        exit("nextExpression");
        return d;
    }

//  NEXT CONJUNCTION. Parse one or more COMPARISONs, separated by AND tokens.

    private Descriptor nextConjunction()
    {
        enter("nextConjunction");
        Descriptor d = nextComparison();
        while (getToken() == boldAndToken)
        {
            nextToken();
            d = nextComparison();
        }
        exit("nextConjunction");
        return d;
    }

//  NEXT COMPARISON. Parse a SUM, optionally followed by a comparison operator,
//  and another SUM. Comparison operators don't associate, so we say IF instead
//  of WHILE.

    private Descriptor nextComparison()
    {
        enter("nextComparison");
        Descriptor d = nextSum();
        if (!d.getType().isSubtype(intType))
        {
            error("int type expected");
        }
        if (isComparisonOperator(getToken()))
        {
            nextToken();
            d = nextSum();
            if (!d.getType().isSubtype(intType))
            {
                error("int type expected");
            }
        }
        exit("nextComparison");
        return d;
    }

//  NEXT SUM. Parse one or more PRODUCTs separated by sum operators.

    private Descriptor nextSum()
    {
        enter("nextSum");
        Descriptor d = nextProduct();
        while (isSumOperator(getToken()))
        {
            nextToken();
            d = nextProduct();
        }
        exit("nextSum");
        return d;
    }

//  NEXT PRODUCT. Parse one or more TERMs separated by product operators.

    private Descriptor nextProduct()
    {
        enter("nextProduct");
        Descriptor d = nextTerm();
        while (isProductOperator(getToken()))
        {
            nextToken();
           d = nextTerm();
        }
        exit("nextProduct");
        return d;
    }

//  NEXT TERM. Parse a term operator followed by another TERM, or else a UNIT.

    private Descriptor nextTerm()
    {
        enter("nextTerm");
        Descriptor d;
        if (isTermOperator(getToken()))
        {
            nextToken();
           d = nextTerm();
        }
        else
        {
            d = nextUnit();
        }
        exit("nextTerm");
        return d;
    }

//  NEXT UNIT. Parse an integer constant, an array element, a procedure call, a
//  name, an EXPRESSION in parentheses, or a string constant. Note that we have
//  an empty DEFAULT that will be filled in later when we add code generation.

    private Descriptor nextUnit()
    {
        enter("nextUnit");
        switch (getToken())
        {
            case intConstantToken:
            {
                nextToken();
                return new Descriptor(intType);
            }
            case nameToken:
            {
                String name = getString();
                nextToken();
                switch (getToken())
                {
                    case openBracketToken:
                    {
                        Descriptor d = table.getDescriptor(name);
                        if (!d.getType().isSubtype(new ArrayType(0, intType)))
                        {
                            error("array type expected");
                        }
                        nextToken();
                        d = nextExpression();
                        if (!d.getType().isSubtype(intType))
                        {
                            error("int type expected");
                        }
                        nextExpected(closeBracketToken);
                        return d;
                    }
                    case openParenToken:
                    {
                        nextArguments();
                        return new Descriptor(new ProcedureType());
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
            }
            case openParenToken:
            {
                nextToken();
                nextExpression();
                nextExpected(closeParenToken);
                return new Descriptor(new ProcedureType());

            }
            case stringConstantToken:
            {
                nextToken();
                return new Descriptor(stringType);
            }
            default:
            {
                error("Unit expected.");
                break;
            }
        }
        exit("nextUnit");
        return new Descriptor(intType);
    }

//  IS COMPARISON OPERATOR. Test if TOKEN is a comparison operator.

    private boolean isComparisonOperator(int token)
    {
        return
                token == equalToken        || token == lessToken         ||
                        token == greaterToken      || token == lessEqualToken    ||
                        token == greaterEqualToken || token == lessGreaterToken;
    }

//  IS PRODUCT OPERATOR. Test if TOKEN is a product operator.

    private boolean isProductOperator(int token)
    {
        return token == slashToken || token == starToken;
    }

//  IS SUM OPERATOR. Test if TOKEN is a sum operator.

    private boolean isSumOperator(int token)
    {
        return token == dashToken || token == plusToken;
    }

//  IS TERM OPERATOR. Test if TOKEN is a term operator.

    private boolean isTermOperator(int token)
    {
        return token == boldNotToken || token == dashToken;
    }

    //Parses all procedures and adds them to the symbol table with their parameter and value types
    private void doPassOne(){
        enter("passOne");
        while (getToken() != endFileToken) //exit if at end of the file
        {
            if (getToken() == boldProcToken) //anytime a bold procedure token is found, add to symbol table
            {
                nextToken();
                String nameAsString = getString();
                nextExpected(nameToken);
                ProcedureType proc = new ProcedureType();
                nextExpected(openParenToken);
                if (getToken() != closeParenToken)
                {
                    //checks for end of parameters list
                    boolean done = false;
                    while (!done) {
                        switch (getToken()) {
                            case boldIntToken: {
                                proc.addParameter(intType);
                                nextToken();
                                nextExpected(nameToken);
                                break;
                            }
                            case boldStringToken: {
                                proc.addParameter(stringType);
                                nextToken();
                                nextExpected(nameToken);
                                break;
                            }
                            case openBracketToken: {
                                nextToken();
                                nextExpected(intConstantToken);
                                nextExpected(closeBracketToken);
                                proc.addParameter(new ArrayType(getInteger(), null));
                                nextExpected(boldIntToken);
                                nextExpected(nameToken);
                                break;
                            }
                            default: {
                                error("Parameter expected.");
                                break;
                            }
                        }
                        if (getToken() == commaToken) {
                            nextToken();
                        } else {
                            done = true;
                        }
                    }

                }
                nextExpected(closeParenToken);
                switch (getToken())
                {
                    case boldIntToken:
                    {
                        proc.addValue(intType);
                        nextToken();
                        break;
                    }
                    case boldStringToken:
                    {
                        proc.addValue(stringType);
                        nextToken();
                        break;
                    }
                    default:
                    {
                        error("Value type expected.");
                        break;
                    }
                }
                nextExpected(colonToken);
                table.setDescriptor(nameAsString, new Descriptor(proc));
            }
            else
            {
                nextToken();
            }
        }
        close();
        reread();
    }
//parses rest of program and closes path file when done
    private void doPassTwo(){
        this.nextProgram();
        close();
    }
//  MAIN. Main program, for testing. Get a file path from the control statement
//  and parse the Snarl program found on that file.

    public static void main(String[] args)
    {
        new Parser(args[0]);
    }

}
