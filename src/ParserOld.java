/**
 * Sabrina Fiester
 */
public class ParserOld extends Scanner {

    //constructor
    public ParserOld(String path)
    {
        super(path);

    }

    //parse a program
    public void nextProgram()
    {
        enter("nextProgram");
        nextPartialProgram();
        while (getToken() == semicolonToken)
        {
            nextToken();
            nextPartialProgram();
        }
        exit("nextProgram");
    }

    private void nextPartialProgram()
    {
        //check for decleration
        if (getToken() == boldIntToken || getToken() == boldStringToken ||
                getToken() == openBracketToken)
        {
            nextDecleration();
        }
        //check for procedure
        else if (getToken() == boldProcToken)
        {
            nextProcedure();
        }
        else
        {
            error("Unexpected token.");
        }
    }

    //parse a procedure
    private void nextProcedure() {
        enter("nextProcedure");
        nextToken();
        nextToken();
        nextToken();
        if (getToken() == closeParenToken) {
            nextToken();
            if (getToken() == intConstantToken || getToken() == stringConstantToken) {
                nextToken();
                if (getToken() == boldBeginToken) {
                    nextStatement();
                } else {
                    nextDecleration();
                    while (getToken() == semicolonToken) {
                        nextDecleration();
                    }
                }
            }
        } else
        {
            nextDecleration();
            while (getToken() == semicolonToken) {
                nextDecleration();
            }
            nextToken();
            if (getToken() == intConstantToken || getToken() == stringConstantToken) {
                nextToken();
                if (getToken() == boldBeginToken) {
                    nextStatement();
                } else {
                    nextDecleration();
                    while (getToken() == semicolonToken) {
                        nextDecleration();
                    }
                }
            }
        }
        exit("nextProcedure");
    }

    //parse a decleration
    private void nextDecleration()
    {
        enter("nextDecleration");
        if (getToken() == openBracketToken)
        {
            nextToken(); //skip the openBracketToken
            nextExpected(intConstantToken);
            nextExpected(closeBracketToken);
            nextExpected(boldIntToken);
        }
        else
        {
            nextToken(); //either skip the boldIntToken or the stringConstantToken
        }
        nextExpected(nameToken);
        exit("nextDecleration");
    }

    //parse a statement
    private void nextStatement()
    {
        enter("nextStatement");
        switch(getToken())
        {
            case nameToken:
            {
                nextAssignOrCall();
                break;
            }
            case boldBeginToken:
            {
                nextBegin();
                break;
            }
            case boldCodeToken:
            {
                nextCode();
                break;
            }
            case boldIfToken:
            {
                nextIf();
                break;
            }
            case boldValueToken:
            {
                nextValue();
                break;
            }
            case boldWhileToken:
            {
                nextWhile();
                break;
            }
            default:
            {
                error("Statement expected.");
            }
        }
        exit("nextStatement");
    }

    //parse an expression
    private void nextExpression()
    {
        enter("nextExpression");
        nextConjunction();
        while (getToken() == boldOrToken)
        {
            nextToken();
            nextConjunction();
        }
        exit("nextExpression");
    }

    //parse assignment or call
    private void nextAssignOrCall()
    {
        enter("nextAssignOrCall");
        nextToken();
        if (getToken() == colonEqualToken)
        {
            nextToken();
            nextExpression();
        }
        else if (getToken() == openBracketToken)
        {
            nextToken();
            nextExpression();
            nextExpected(closeBracketToken);
            nextExpected(colonEqualToken);
            nextExpression();
        }
        else if (getToken() == openParenToken)
        {
            nextToken();
            if (getToken() != closeParenToken)
            {
                nextExpression();
                while (getToken() == commaToken)
                {
                    nextToken();
                    nextExpression();
                }
                nextExpected(closeParenToken);
            }
            else
            {
                nextToken();
            }
        }
        exit("nextAssignOrCall");
    }

    //parse a begin statement
    private void nextBegin()
    {
        enter("nextBegin");
        nextToken();
        if (getToken() != boldEndToken)
        {
            nextStatement();
            while (getToken() == semicolonToken)
            {
                nextToken();
                nextStatement();
            }
            nextExpected(boldEndToken);
        }
        else
        {
            nextToken();
        }
        exit("nextBegin");
    }

    //parse code statement
    private void nextCode()
    {
        enter("nextCode");
        nextToken();
        nextExpected(stringConstantToken);
        exit("nextCode");
    }

    //parse if statement
    private void nextIf()
    {
        enter("nextIf");
        while(getToken() == boldIfToken)
        {
            nextToken();
            nextExpression();
            nextExpected(boldThenToken);
            nextStatement();
            if (getToken() == boldElseToken)
            {
                nextToken();
                if (getToken() != boldIfToken)
                {
                    nextStatement();
                    break;
                }
            }
        }
        exit("nextIf");
    }

    //parse a value statement
    private void nextValue()
    {
        enter("nextValue");
        nextToken();
        nextExpression();
        exit("nextValue");
    }

    //parse a conjunction
    private void nextConjunction()
    {
        enter("nextConjunction");
        nextComparison();
        while (getToken() == boldAndToken)
        {
            nextToken();
            nextComparison();
        }
        exit("nextConjunction");
    }

    //parse a comparison
    private void nextComparison()
    {
        enter("nextComparison");
        nextSum();
        if (isComparisonOperator(getToken()))
        {
            nextToken(); //skip the comparison
        }
        exit("nextComparison");

    }

    //parse a sum
    private void nextSum()
    {
        enter("nextSume");
        nextProduct();
        while (getToken() == plusToken || getToken() == dashToken)
        {
            nextToken();
            nextProduct();
        }
        exit("nextSum");
    }

    //parse a product
    private void nextProduct()
    {
        enter("nextProduct");
        nextTerm();
        while (getToken() ==  starToken|| getToken() == slashToken)
        {
            nextToken();
            nextTerm();
        }
        exit("nextProduct");
    }

    //parse a term
    private void nextTerm()
    {
        enter("nextTerm");
        if (getToken() == dashToken || getToken() == boldNotToken)
        {
            nextToken();
            nextTerm();
        }
        else
        {
            nextUnit();
        }
        exit("nextTerm");
    }


    //check if a token is a comparison operator
    private boolean isComparisonOperator(int token)
    {
        return token == greaterToken || token == greaterEqualToken || token == lessToken ||
                token == lessEqualToken || token == lessGreaterToken || token == equalToken;
    }

    //parse a unit
    private void nextUnit()
    {
        enter("nextUnit");
        switch(getToken())
        {
            case nameToken:
            {
                nextToken();
                if (getToken() == openParenToken)
                {
                    nextToken();
                    if (getToken() != closeParenToken)
                    {
                        nextExpression();
                        while (getToken() == commaToken)
                        {
                          nextToken();
                           nextExpression();
                        }
                        nextExpected(closeParenToken);
                    }
                    else
                    {
                        nextToken();
                    }
                }
                else if (getToken() == openBracketToken)
                {
                    nextToken();
                    nextExpression();
                    nextExpected(closeBracketToken);
                }
                break;
            }
            case openParenToken:
            {
                nextToken();
                nextExpression();
                nextExpected(closeParenToken);
                break;
            }
            case intConstantToken:
            {
                nextToken();
                break;
            }
            case stringConstantToken:
            {
                nextToken();
                break;
            }
            default:
            {
                error("Unexpected token.");
            }
        }
        exit("nextUnit");
    }

    //parse while
    private void nextWhile()
    {
        enter("nextWhile");
        nextToken(); //skip the boldWhileToken
        nextExpression();
        nextExpected(boldDoToken);
        nextStatement();
        exit("nextWhile");
    }

    //get next token and check that it is correct
    private void nextExpected(int token)
    {
        if (getToken() == token)
        {
            nextToken();
        }
        else
        {
            error(tokenToString(token) + "expected.");
        }
    }

    public static void main(String[] args)
    {
        ParserOld parse = new ParserOld(args[1]);
        parse.nextProgram();
    }


}
