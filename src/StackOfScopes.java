/**
 * Sabrina Fiester
 * Linked stack of scopes implementation
 */
public class StackOfScopes {
    private Scope top; //top scope of stack

    //constructor. creates empty stack
    public StackOfScopes() {
        top = null;
    }

    //puts a new, empty scope onto the stack
    public void push() {
        //temporary copy of top node
        Scope hold = top;
        top = new Scope(hold);
    }

    //pop top scope off of stack
    public void pop() {
        if (isEmpty()) {
            throw new IllegalStateException("Stack is empty");
        } else {
            top = top.getNext();
        }
    }

    public boolean isEmpty()
    {
        return top == null;
    }
    //returns the top scope
    public Scope getTop() {
        return top;
    }


}
