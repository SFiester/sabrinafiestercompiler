import java.util.Hashtable;
/**
 * Sabrina Fiester
 * HW 2
 */
class ScannerOld extends Source
{
    //Hashtable to hold reserved words
    private Hashtable<String, Integer> reserved;
    //String of current token
    private String tokenString;
    //current token
    private int token;
    //int associated with current token
    private int tokenInt;

    //Creates new instance of Scanner with a hashtable of reserved words
    public ScannerOld(String path)
    {
        super(path);
        //initialize hashtable of reserved words
        reserved=new Hashtable<String, Integer>();
        //add the reserved words to the hashtable
        reserved.put("and", boldAndToken);
        reserved.put("begin", boldBeginToken);
        reserved.put("code", boldCodeToken);
        reserved.put("do", boldDoToken);
        reserved.put("else", boldElseToken);
        reserved.put( "end", boldEndToken);
        reserved.put("if", boldIfToken);
        reserved.put("int", boldIntToken);
        reserved.put("not", boldNotToken);
        reserved.put("or", boldOrToken);
        reserved.put("proc", boldProcToken);
        reserved.put("string", boldStringToken);
        reserved.put("then", boldThenToken);
        reserved.put("value", boldValueToken);
        reserved.put("while", boldWhileToken);
        nextToken();
    }

    //returns current token
    public int getToken()
    {
        return token;
    }

    //returns int associated with current token
    public int getInt()
    {
        return tokenInt;
    }

    //returns string associated with current token
    public String getString()
    {
        return tokenString;
    }

    //gets the next token from input
    public void nextToken()
    {
        switch (getChar())
        {

            //Check for boldAndToken
            case '\u2227':
            {
                token = boldAndToken;
                nextChar();
                break;
            }
            //Check for boldOrToken
            case '\u2228':
            {
                token = boldOrToken;
                nextChar();
                break;
            }
            //check for closeBracketToken
            case ']':
            {
                token = closeBracketToken;
                nextChar();
                break;
            }
            //check for closeParenToken
            case ')':
            {
                token = closeParenToken;
                nextChar();
                break;
            }
            //check for either colonToken or colonEqualToken
            case ':':
            {
                nextChar();
                if (getChar() == '=')
                {
                    token = colonEqualToken;
                }
                else
                {
                    token = colonToken;
                }
            }
            //check for commaToken
            case ',':
            {
                token = commaToken;
                nextChar();
                break;
            }
            //check for dashToken
            case '-':
            {
                token = dashToken;
                nextChar();
                break;
            }
            //check for end of file
            case eofChar:
            {
                token = endFileToken;
                break;
            }
            //check for equalToken
            case '=':
            {
                token = equalToken;
                nextChar();
                break;
            }
            //check for greaterEqualToken
            case '\u2265':
            {
                token = greaterEqualToken;
                nextChar();
                break;
            }
            //check for tokens beginning with >
            case '>':
            {
                nextGreater();
                break;
            }
            //check for ignoredToken
            case ' ':
            {
                token = ignoredToken;
                nextChar();
                break;
            }
            //check for comment
            case '#':
            {
                nextComment();
                break;
            }
            //check for lessEqualToken
            case '\u2264':
            {
                token = lessEqualToken;
                nextChar();
                break;
            }
            //check for lessGreaterToken
            case '\u2260':
            {
                token = lessGreaterToken;
                nextChar();
                break;
            }
            //check for tokens beginning with <
            case '<':
            {
                nextLess();
                break;
            }
            //check for openBracketToken
            case '[':
            {
                token = openBracketToken;
                nextChar();
                break;
            }
            //check for openParenToken
            case '(':
            {
                token = openParenToken;
                nextChar();
                break;
            }
            //check for plusToken
            case '+':
            {
                token = plusToken;
                nextChar();
                break;
            }
            //check for semicolonToken
            case ';':
            {
                token = semicolonToken;
                nextChar();
                break;
            }
            //check for slashToken
            case '/':
            {
                token = slashToken;
                nextChar();
                break;
            }
            //check for starToken
            case '*':
            {
                token = starToken;
                nextChar();
                break;
            }
            //check for multiplication sign
            case '\u00D7':
            {
                token = starToken;
                nextChar();
                break;
            }
            //Check for stringConstantToken
            case '"':
            {
                nextString();
                break;
            }
            default:
            {
                //check for intConstantToken
                if (isDigit(getChar()))
                {
                    nextNumber();
                }
                //check for nameToken
                else if (isLetter(getChar()))
                {
                    nextName();
                }
                else
                {
                    error("Token not found.");
                }
            }
        }
    }

    //move through comment
    private void nextComment()
    {
        while (!atLineEnd())
        {
            nextChar();
        }
        token = ignoredToken;
        nextChar();
    }

    //determine next name token and tokenString
    private void nextName()
    {
        StringBuilder builder = new StringBuilder();
        while(isLetter(getChar()) || isDigit(getChar()))
        {
            builder.append(getChar());
            nextChar();
        }
        tokenString = builder.toString();
        if(reserved.containsKey(tokenString))
        {
            token = reserved.get(tokenString);
        }
        else
        {
            token = nameToken;
        }
    }

    //get number from input
    public void nextNumber()
    {
        StringBuilder builder = new StringBuilder();
        while(isDigit(getChar()))
        {
            builder.append(getChar());
            nextChar();
        }
        //Detect if there is an integer error
        try
        {
            tokenInt = Integer.parseInt(builder.toString());
        }
        catch (NumberFormatException e)
        {
            error("Error: Invalid number.");
        }
        tokenString = builder.toString();
        token = intConstantToken;
    }

    //checks if ch is between '0' and '9'
    private boolean isDigit(char ch)
    {
        return '0' <=ch && ch<= '9';
    }

    //checks if ch is a letter
    private boolean isLetter(char ch)
    {
        return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z';
    }

    //gets next string from input
    public void nextString()
    {
        StringBuilder builder = new StringBuilder();
        nextChar(); //to skip opening "
        while (getChar() != '"' && !atLineEnd()) //Quote hasn't ended, not at end of line
        {
            builder.append(getChar());
            nextChar();
        }
        if(atLineEnd())
        {
            error("Missing closing quote");
        }
        else
        {
            nextChar(); //skip "
            tokenString = builder.toString();
            token = stringConstantToken;
        }
    }

    //gets next token that begins with <
    private void nextLess()
    {
        nextChar();
        if (getChar() == '>')
        {
            token = lessGreaterToken;
            nextChar();
        }
        else if (getChar() == '=')
        {
            token = lessEqualToken;
            nextChar();
        }
        else
        {
            token = lessToken;
        }
    }

    //gets next token that begins with >
    private void nextGreater()
    {
        nextChar();
        if (getChar() == '=')
        {
            token = greaterEqualToken;
            nextChar();
        }
        else
        {
            token = greaterToken;
        }
    }

}
