/**
 * Sabrina Fiester
 * Scope object implementation
 * linked stack
 */
public class Scope {

    private Scope next; //scope below this one in stack
    private Descriptor desc; //descriptor associated with this stack
    //initializes new, empty scope
    public Scope(Scope newNext)
    {
        desc = null;
        //creates link
        this.next = newNext;
    }

    //returns the descriptor for this scope
    public Descriptor getDesc() {
        return desc;
    }

    //returns the next scope on the stack
    public Scope getNext() {
        return next;
    }

    //sets this scope's descriptor to newDesc
    public void setDesc(Descriptor newDesc){
        this.desc = newDesc;
    }
}
