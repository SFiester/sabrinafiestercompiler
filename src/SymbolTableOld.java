import java.util.Hashtable;

/**
*Sabrina Fiester
* Symbol table implementation
 */
public class SymbolTableOld {
    private Hashtable<String, StackOfScopes> table;//hash table to hold all names and scopes
    private int level; //keeps track of current level

    //creates an empty symbol table
    public SymbolTableOld(){
        table = new Hashtable<String, StackOfScopes>();
        level = 0;
    }
    //tests if table is empty
    public boolean isEmpty(){
        return level < 1;
    }
    //add new scope to top of table
    public void push(){
        level++;
    }
    //pops scope off top of symbol table
    public void pop(){
        if (this.isEmpty()){
            throw new IllegalStateException("This table is empty.");
        }
        else{
            level--;
        }
    }

    //checks if name is declared in current scope
    public boolean isDeclared(String name){
        return table.get(name).getSize() == level;
    }
    //returns descriptor at top of scope stack for name
    public Descriptor getDescriptor(String name){
        if (this.isEmpty()){
            throw new IllegalStateException("This table is empty.");
        }
        else if (!table.containsKey(name) || table.get(name).getSize() < 1){
            return null;
        }
        else{
            return table.get(name).getTop().getDesc();
        }
    }
    //sets descriptor at current level
    public Boolean setDescriptor(String name, Descriptor descriptor){
        if (this.isEmpty()){
            throw new IllegalStateException("This table is empty.");
        }
        else{
            table.get(name).push();
            if(table.get(name).getTop()==null){
                table.get(name).getTop().setDesc(descriptor);
                return true;
            }

        }
        return false;
    }

}
